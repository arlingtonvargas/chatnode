const socket = io();
var myApp = angular.module('App',[]);

myApp.controller('mainController', ['$scope', function($scope) {
 
    
    var output = $('#output');
    var actions = $('#actions');

    $scope.showIngreso = true;
    $scope.showChat = false;
    $scope.startchat = false;
    $scope.height = 300;
    $scope.currentuser = {};

    // #region Functions

 
    $scope.InsertMessage = function InsertMessage(message){
        if(message === "")return;
        socket.emit('chat:message',{
            message: message,
            username: $scope.currentuser.username
        });   
        $scope.txtmessage = "";
        socket.emit('chat:typingout',true);     
    }

    $scope.InsertMessageEnter = function InsertMessageEnter(message, event){
        if(message === "" || event.keyCode != 13)return;
        $scope.InsertMessage(message);
    }

    $scope.InNewUser = function InNewUser(ptxtuserdoc, ptxtusername){
        if(ptxtuserdoc === "" || ptxtuserdoc === undefined || ptxtusername === "" || ptxtusername === undefined)return;        
        username = ptxtusername;
        socket.emit('chat:newuser', {
            document: ptxtuserdoc,
            username: ptxtusername
        });
        var wcurrentuser = { 'document': ptxtuserdoc, 'username': ptxtusername};
        $scope.currentuser = wcurrentuser;
        localStorage.setItem('currentuser', JSON.stringify(wcurrentuser));
        $scope.txtuserdoc = "";
        $scope.txtusername = "";
        $scope.showIngreso = false;
        $scope.showChat = true;
        $scope.startchat = true;   
    }

    $scope.ClearFields = function ClearFields(){
        $scope.txtusername = "";
        $scope.txtuserdoc = "";
        $scope.txtmessage = "";
        actions.empty();
        output.empty();
        $scope.showIngreso = true;
        $scope.showChat = false;
        $scope.startchat = false;   
        $scope.currentuser = null;
        localStorage.removeItem('currentuser');
    }

    $scope.Typing= function(){
        socket.emit('chat:typing',$scope.currentuser.username);
    }

    $scope.CloseChat = function(){
        var res = confirm("Seguro que desea abandonar el chat?");
        if(res === true)
        {
            socket.emit('chat:closechat', $scope.currentuser);
            $scope.ClearFields();
            output.empty();
        }
    }

    $scope.TypingOut = function(){
        socket.emit('chat:typingout',true);
    }

    // #endregion
   

    // #region Comunication Socket

    socket.on('chat:message', function(data){
        if($scope.startchat === false)return;
        output.append('<p><strong>' + data.username + '</strong>: ' + data.message + '</p>')
        $scope.height += 100;
        output.scrollTop($scope.height);  
    });

    socket.on('chat:typing', function(data){
        if($scope.startchat === false)return;
        actions.empty();
        actions.append('<p><em>' + data + ' esta escribiendo...</em></p>');
    });

    socket.on('chat:typingout', function(data){
        if(data === true)actions.empty();
    });    

    socket.on('chat:newuser', function(data){
        if($scope.startchat === false)return;
        if(data.username === $scope.currentuser.username)
        {
            output.append('<p><strong>Bienvenido ' + data.username + '</strong></p>');
        }
        else
        {
            output.append('<p><strong>' + data.username + '</strong>  acaba de ingresar al chat</p>');            
        }
        $scope.height += 100;
        output.scrollTop($scope.height);  
    });

    socket.on('chat:users', (data) =>{       
        console.log(data);
    });

    socket.on('chat:imagen', function(data){
        output.append('<p><strong>' + data.username + ': </strong><img class = "imgchat" src="'+ data.image +'" />  </p>');            
        $scope.height += 600;
        output.scrollTop($scope.height); 
    });  

    socket.on('chat:file', function(data){
        output.append('<p><strong>' + data.username + ': </strong> <object data="' + data.file + '" width="95%" height="375" '+
        'type="application/pdf"></object> </p>');      
             
        $scope.height += 600;
        output.scrollTop($scope.height); 
    }); 
    
    socket.on('chat:userclosechat', function(data){
        output.append('<p><strong>' + data.username + '</strong>  abandono el chat</p>');            
        $scope.height += 40;
        output.scrollTop($scope.height); 
    });     

    // #endregion

    // #region JQuery

    $(document).ready(function(){

        var ofdloadimage = $('#ofdloadimage');
        var ofdloadfile = $('#ofdloadfile');

        ofdloadimage.on('change', function(e){
            var file = ofdloadimage[0].files[0];
            var reader = new FileReader();
            reader.onload = function(evt){
                socket.emit('chat:imagen',{
                    username: $scope.currentuser.username,
                    image: evt.target.result
                });
            };
            reader.readAsDataURL(file);
        });

        ofdloadfile.on('change', function(e){
            var file = ofdloadfile[0].files[0];
            var reader = new FileReader();
            reader.onload = function(evt){
                socket.emit('chat:file',{
                    username: $scope.currentuser.username,
                    file: evt.target.result,
                    type: file.type
                });
            };
            reader.readAsDataURL(file);
        });
    });

    // #endregion

}]);
