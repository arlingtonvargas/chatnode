const path = require('path');
const express = require('express');
const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, '../client')));

const server = app.listen(app.get('port'),()=>{
    console.log('server ok port', app.get('port'));
});


const SocketIO = require('socket.io');
const io = SocketIO(server);
var usuarios = [];

io.on('connection', (socket)=>{
    socket.on('chat:message', (data) =>{
        io.sockets.emit('chat:message', data);
    });

    socket.on('chat:newuser', (data) =>{
        io.sockets.emit('chat:newuser', data);
        usuarios.push({
            document: data.document,
            username: data.username
        });
        io.sockets.emit('chat:users', usuarios);
    }); 

    socket.on('chat:closechat', (data) =>{
        for( var i = 0; i < usuarios.length; i++){ 
            if ( usuarios[i].document === data.document) {
                usuarios.splice(i, 1); 
                io.sockets.emit('chat:userclosechat', data);
                break;
            }
         }
    }); 

    socket.on('chat:typing', (data) =>{
        socket.broadcast.emit('chat:typing',data);
    });

    socket.on('chat:typingout', (data) =>{
        socket.broadcast.emit('chat:typingout',data);
    });

    socket.on('chat:imagen', (data) =>{
        io.sockets.emit('chat:imagen',data);
    });

    socket.on('chat:file', (data) =>{        
        io.sockets.emit('chat:file',data);
    });


});
